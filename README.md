# ds_pytools, a python package to control the DeeSse software #


### What is this repository for? ###

DS_pytools is apython package to control the DeeSse software for MultiplePoint Statistics (MPS) simulation (http://www.randlab.org/).
It contains functions to write and read .gslib files used by DS to read the training image and results.

Version 0.1 Beta, Jul 2018, Fabio Oriani, University of Lausanne
fabio (dot) oriani (at) protonmail (dot com)

### How to begin ###

To begin using the software:

_ contact the Randlab team (http://www.randlab.org/) to obtain and install the DeeSse software (see license terms, free for research use);

_ add ds_pytools to your python path or in your work directory;

_ open ./example/example.py and follow the example.

### Dependencies ###

_ Python 3

_ Python packages: numpy, matplotlib, subprocess

### Need help? ###

Write Fabio : fabio (dot) oriani (at) protonmail (dot com)

### How to cite this software ###

DeeSse implementation:
Straubhaar J (2017) DeeSse user’s guide. CHYN–stochastic hydrogeology and geostatistics. University of Neuchâtel, Switzerland, p 36

Algorithm:
Mariethoz G, Renard P, Straubhaar J (2010) The direct sampling method to perform multiple-point geostatistical simulations. Water Resour Res 46(W11536):1–14


### License for ds_pytools ###

Copyright (C) 2018  Fabio Oriani

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
