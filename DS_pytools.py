#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
DS TOOLS
set of functions to use the DeeSse (randlab.org) software from python

Created on Thu Jun 14 11:24:49 2018

@author: Fabio Oriani, fabio.oriani <at> protonmail.com
"""
# required packages
import numpy as np

def write_gslib(data,filename,nvar,names):
    """
     WRITE_GSLIB writes one or more variables in .gslib format for DS.
     
     SYNTAX: write_gslib(data,filename,names)
     
     INPUT: 
         data: numpy array containing the data to write, max 3D with 4-th 
         dimension as number of variables.
         filename: the name of the file written.
         nvar: number of variables contained in the dataset
         names: list of strings containing the variables names.
    
     OUTPUT: 
         GSLIB file in local directory 
     
     VERSION 0.1
     Fabio Oriani, 06/2018
     Unil, fabio.oriani <at> protonmail.com
    """
    
    f = open(filename, 'wb') # create and open ascii file
    if nvar>1: # multivariate case
        # PRINT HEADER
        sdata=np.shape(data) # data size (with vars number as last dim)
        dim_num=np.ndim(data)-1 # data set dimensionality
        data_size=np.array([[1,1,1,1,1,1,0,0,0]]) # data size (without vars number) + spacing (1 by default) + origin coordinates (0 by default)
        data_size[0,0:dim_num]=sdata[:-1] 
        np.savetxt(f,data_size,fmt='%i')
        np.savetxt(f,[sdata[-1]],fmt='%i') # number of variables
        for i in range(len(names)): # print var names
            f.write(bytes('%s \n' % names[i], encoding='ascii'))
      #      f.write(bytes('%s\n' % names[i], encoding='ascii'))
        # PRINT DATA
        data=np.reshape(data,[np.prod(data_size[0,0:2]),sdata[-1]],order='F')
        np.savetxt(f,data)
        f.close()
    else: # univariate case
      # PRINT HEADER
        sdata=np.shape(data) # data size
        dim_num=np.ndim(data) # data set dimensionality
        data_size=np.array([[1,1,1,1,1,1,0,0,0]]) # data size (without vars number) + spacing + origin
        data_size[0,0:dim_num]=sdata 
        np.savetxt(f,data_size,fmt='%i')
        f.write(bytes('1 \n', encoding='ascii')) # number of variables
        f.write(bytes('%s \n' % names, encoding='ascii'))
        # PRINT DATA
        data=np.reshape(data,np.prod(data_size[0,0:2]),order='F')
        np.savetxt(f,data)
        f.close()
        
def read_gslib(filename):
    '''
    READ_GSLIB imports variables from Gslib file for DS.
    
    USAGE: 
        out_names,out_data=read_gslib(filename) output all variables contained
        in the gslib file into a numpy array and the names in a list
    
    INPUT:
        filename = string variables indicating the filename or path/filename 
        to import
    
    OUTPUT:
        1) numpy array of dimensionality N, being N the dimensionality of 
        the dataset, +1 if multivariate 
        
        2) list containing the variable names
    
    VERSION 0.1
        Fabio Oriani, 06/2018
        Unil, fabio.oriani <at> protonmail.com
    '''
    
    f=open(filename)
    tmp=f.readline() # data size
    tmp=tmp[:-1].split(' ')
    data_size=[int(tmp[0]),int(tmp[1]),int(tmp[2])]
    tmp=f.readline() # number of variables
    nvar=int(tmp[0])
    varnames=[] # variable names
    for i in range(nvar):
        tmp=f.readline()
        varnames.append(tmp[:-1])
    out=np.genfromtxt(f,)
    f.close()
    out=np.squeeze(np.reshape(out,np.append(data_size,nvar),order='F'))
    out[out==-9999999]=np.nan
    return out,varnames
    
# TRIANGULAR FUNCTION
def triang(x,phase=0,length=1,amplitude=1):
    '''
    TRIANG creates a periodic triangular function, useful as a 1-D auxiliary 
    variable to indicate periodicity.
    
    USAGE: 
        tr=triang(x,phase,length,amplitude) generates a triangular 1-D signal 
        computed at values in x, with given phase, wave lentgh, and amplitude
    
    INPUT:
        x = 1-D numpy array or numeric list containing the values at which the 
        function is computed
        
        phase = scalar indicating the wave phase (default = 0)
        
        length = scalar indicating the wave length (default = 1)
        
        amplitude = scalar indicating the wave amplitude (default = 1)
    
    OUTPUT:
        1-D numpy array of the same legth of x containing the triangular 
        function values
    
    EXAMPLE:
        import ds_pytools.DS_pytools as ds 
        import matplotlib.pylab as plt

        phase=10
        length=30 # should be positive
        amplitude=10
        x=np.arange(0,100,0.1)
        tr=ds.triang(x,phase,length,amplitude)
        plt.plot(x,tr)
        
    VERSION 0.1
        Fabio Oriani, 06/2018
        Unil, fabio.oriani <at> protonmail.com
    '''
    alpha=(amplitude)/(length/2)
    return -amplitude/2+amplitude*((x-phase)%length==length/2) \
            +alpha*((x-phase)%(length/2))*((x-phase)%length<=length/2) \
            +(amplitude-alpha*((x-phase)%(length/2)))*((x-phase)%length>length/2)
            