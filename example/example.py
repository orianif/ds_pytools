#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EXAMPLE OF DEESSE USAGE FROM PYTHON USING DS_PYTOOLS

Created on Wed Jun 27 11:48:00 2018

@author: Fabio Oriani, fabio.oriani <at> protonmail.com
"""

import subprocess as sp
import numpy as np
import matplotlib.pyplot as plt
import ds_pytools.DS_pytools as ds

#%% GENERATE 2 CORRELATED VARIABLES
var_size=[350,150] # variable size
np.random.seed(400)# random noise
ran=np.random.rand(var_size[0],var_size[1])

# VARIABLE 0
# kernel
lx=40
ly=40
std=5
ax = np.arange(-lx // 2 + 1., lx // 2 + 1.)
ay = np.arange(-ly // 2 + 1., ly // 2 + 1.)
xx, yy = np.meshgrid(ay, ax)
kernel = np.exp(-(xx**2 + yy**2) / (2. * std**2))
kernel= kernel/np.sum(kernel)

#convolution
var0=np.zeros_like(ran)
vartmp=np.pad(ran,((0,ly),(0,lx)),mode='wrap')
for i in range(var_size[0]):
    for j in range(var_size[1]):
        tmp=np.sum(vartmp[i:i+ly,j:j+ly]*kernel)
        var0[i,j]=tmp

# VARIABLE 1
# kernel
lx=10
ly=10
std=80
ax = np.arange(-lx // 2 + 1., lx // 2 + 1.)
ay = np.arange(-ly // 2 + 1., ly // 2 + 1.)
xx, yy = np.meshgrid(ay, ax)
kernel = np.exp(-(xx**2 + yy**2) / (2. * std**2))
kernel= kernel/np.sum(kernel)

#convolution
var1=np.zeros_like(ran)
vartmp=np.pad(ran,((0,ly),(0,lx)),mode='wrap')
for i in range(var_size[0]):
    for j in range(var_size[1]):
        tmp=np.sum(vartmp[i:i+ly,j:j+ly]*kernel)
        var1[i,j]=tmp

# show the variables        
plt.figure()
plt.subplot(1,2,1)
plt.imshow(var0, interpolation='none')
plt.title('VARIABLE 0')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(var1, interpolation='none')
plt.title('VARIABLE 1')
plt.colorbar()
plt.show()

#%% WRITE TI GSLIB FILE
filename="Ti.gslib" # file name
nvar=2 # number fo variables
varnames=['var0','var1'] # variable names
Ti=np.empty([var_size[0],var_size[1],nvar])
Ti[:,:,0]=var0
Ti[:,:,1]=var1
Ti[np.isnan(Ti)]=-9999999
ds.write_gslib(Ti,filename,nvar,varnames)

#%% LAUNCH DEESSE
#cmd='deesse test.in' # instead of "deesee" put the path or link to your deesse executable 
cmd='deesseOMP 8 example.in' # for parallel computing
p=sp.Popen(cmd, shell=True, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.STDOUT, close_fds=True)
print('running simulation...')
for line in iter(p.stdout.readline, b''):print(line,)
print('simulation completed')

# IMPORT SIMULATION
simdata,varnames=ds.read_gslib("test_real00000.gslib") # filename is setup in the DeeSee params file test.in

# show TI and simulation

# show the variables        
plt.figure()
plt.subplot(2,2,1)
plt.imshow(var0, interpolation='none')
plt.title('TI - VARIABLE 0')
plt.colorbar()
plt.subplot(2,2,2)
plt.imshow(var1, interpolation='none')
plt.title('TI - VARIABLE 1')
plt.colorbar()
plt.subplot(2,2,3)
plt.imshow(simdata[:,:,0], interpolation='none')
plt.title(varnames[0])
plt.colorbar()
plt.subplot(2,2,4)
plt.imshow(simdata[:,:,1], interpolation='none')
plt.title(varnames[1])
plt.colorbar()
plt.show()
